#include <iostream>
#include <cstdlib>

int generator(int number, int hypoth){

	int a,b,c,i=0,j,ans=0,group=0, repeat_suspected=0;
	int a_ans[number];
	int b_ans[number];
	int c_ans[number];
	int guessed[hypoth];
	bool unique=false;

	for (j=0;j<=number;j++){
		a_ans[j]=0;
		b_ans[j]=0;
		c_ans[j]=0;
	}
	for (j=0;j<=hypoth;j++){
		guessed[j]=0;
	}

	while (number>0){
		unique=false;
		while(unique == false){
			c=rand()%(hypoth-1)+1;
			unique=true;
			for (j=0;j<=hypoth-1;j++){
				if (c==guessed[j]){
					unique=false;
					break;
				}
			}
		}

		guessed[i]=c;
		i++;
		
		for (a=1;a<=hypoth-2;a++){
			for (b=1;a*a+b*b<=c*c; b++){
				if (a*a+b*b==c*c){
					for (j=0;j<=number;j++){
						if (c_ans[j]==c) {
							repeat_suspected=j;
						}
					}
						if (a_ans[repeat_suspected]==a or a_ans[repeat_suspected]==b){
						}else{
							std::cout<<"sides: "<<a<<" "<<b<<" "<<c<<std::endl;
							a_ans[ans]=a;
							b_ans[ans]=b;
							c_ans[ans]=c;
							number--;
							ans++;
							goto keep;
						}					
				}
			}
			keep:;
		}
		if (i==hypoth-1){
			break;
		}
	}
	return 0;
}

int main(){
	int triangle_number, hypotenuse;
	char confirm;
	std::cout << "How many triangles to generate"<<std::endl
			  <<"(program will generate this many or fewer if limited by hypotenuse)? ";
	std::cin >> triangle_number;
	std::cout << "How long is max hypotenuse? ";
	std::cin >> hypotenuse;
	generator(triangle_number, hypotenuse);
	std::cout << "Type something & Press enter to exit! ";
	std::cin >> confirm;
	
	return 0;
}